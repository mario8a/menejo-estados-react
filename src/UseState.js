import React, { useEffect, useState } from 'react'

const SECURITY_CODE = 'mama';

function UseState({name}) {

  const [state, setState] = useState({
    value: '',
    error: false,
    loading: false,
    deleted: false,
    confirm: false
  });
  // ESTADOS INDEPENDEINETES
  // const [value, setValue] = useState('');
  // const [error, setError] = useState(false);
  // const [loading, setLoading] = useState(false);

  const onConfirm = () => {
    setState({
      ...state,
      loading: false,
      error: false,
      confirm: true
    })
  };

  const onError = () => {
    setState({
      ...state,
      error: true,
      loading: false,
    })
  };

  const onWrite = (newValue) => {
    setState({
      ...state,
      value: newValue
    })
  };

  const onCheck = () => {
    setState({
      ...state,
      loading: true
    })
  };

  const onDelete = () => {
    setState({...state,deleted: true});
  };

  const onReset = () => {
    setState({...state,confirm: false, deleted: false, value: ''});
  }

  useEffect(() => {
    if (!!state.loading) {
      setTimeout(() => {
        if (state.value === SECURITY_CODE ) {
          onConfirm();
          // setLoading(false)
          // setError(false)
        } else {
          onError();
          // setError(true)
          // setLoading(false);
        }
      }, 3000);
    }
  }, [state.loading])

  if (!state.deleted && !state.confirm) {
    return (
      <div>
        <h2>Eliminar {name}</h2>
        <p>Por favor, escriba el código de seguridad.</p>
  
        {(state.error && !state.loading) && (
          <p>Error: El codigo es incorrecto</p>
        )}
  
        {state.loading && (
          <p>Cargando ...</p>
        )}
  
        <input 
          type='text' 
          placeholder='código de seguridad'
          value={state.value}
          onChange={(event) => {
            // setError(false);
            onWrite(event.target.value);
            // setValue(event.target.value);
          }}  
        />
        <button
          onClick={() => {
            // setError(false);
            onCheck();
            // setLoading(!loading);
          }}
        >Comprobar</button>
      </div>
    )
  } else if (!!state.confirm && !state.deleted) {
    return (
      <>
        <p>¿Segudo que quieres eliminar useState?</p>
        <button 
          onClick={() => {
            onDelete();
          }}
        >Si, eliminar</button>
        <button
          onClick={() => {
            onReset();
          }}
        >No, regresar</button>
      </>
    )
  } else {
    return (
      <>
        <p>Eliminado con exito</p>
        <button
          onClick={() => {
            onReset();
          }}
        >Volver al useState</button>
      </>
    )
  }
}

export {UseState}