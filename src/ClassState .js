import React, { Component } from 'react'

const SECURITY_CODE = 'mama';

export default class ClassState extends Component {

  constructor(props) {
    super(props);
    this.state = {
      value: '',
      error: false,
      loading: false
    };
  }

  // componentWillMount() {
  //   //Se ejecuta antes de renderizar los componentes
  // }

  // componentDidMount() {
  //   //Cuando los componentes ya estan renderizados
  // }

  // componentWillUnmount() {
  //   //uando se desrenderiza (cambiamos de componente)
  // }

  componentDidUpdate() {
    // Se actualiza el componente
    if (!!this.state.loading) {
      setTimeout(() => {
      
        if (SECURITY_CODE === this.state.value) {
          this.setState({error:false, loading: false});
        } else {
          this.setState({error: true, loading: false});
        }
      
      }, 2000);
    }
  }

  render() {

    // const {error, loading, value} = this.state;

    return (
      <div>
        <h2>Eliminar {this.props.name}</h2>
        <p>Por favor, escriba el código de seguridad.</p>

        {((this.state.error && !this.state.loading)) && (
         <p>Error: El codigo es incorrecto</p>
        )}

        {this.state.loading && (
         <p>Cargando ...</p>
        )}
        
        <input 
          type='text' 
          placeholder='código de seguridad'
          value={this.state.value}
          onChange={(event) => {
            this.setState({value: event.target.value})
          }}
        />
        <button
          onClick={() => this.setState({loading: !this.state.loading})}
        >Comprobar</button>
      </div>
    )
  }
}