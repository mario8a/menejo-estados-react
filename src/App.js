import React from 'react'
import './App.css'
import ClassState from './ClassState ';
import { UseReducer } from './UseReducer';
import { UseState } from './UseState';


function App() {
  return (
    <div className='App'>
      <UseState  name ="use State" />
      <UseReducer name ="Use reducer" />
    </div>
  )
}

export default App;