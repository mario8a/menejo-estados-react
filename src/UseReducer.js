import React, { useEffect, useReducer } from 'react'

const SECURITY_CODE = 'mama';

function UseReducer({name}) {

  const [state, dispatch] = useReducer(reducer, initialState);

  const onConfirm = () => {
    dispatch({type: actionTypes.confirm})
  };

  const onError = () => {
    dispatch({type: actionTypes.error})
  };

  const onWrite = (newValue) => {
    dispatch({ type: actionTypes.write, payload: newValue });
  };

  const onCheck = () => {
    dispatch({type: actionTypes.check})
  };

  const onDelete = () => {
    dispatch({type: actionTypes.delete})
  };

  const onReset = () => {
    dispatch({type: actionTypes.reset})
  }

  useEffect(() => {
    if (!!state.loading) {
      setTimeout(() => {
        if (state.value === SECURITY_CODE ) {
          // dispatch({ type: actionTypes.confirm });
          onConfirm();
        } else {
          // dispatch({ type: actionTypes.error });
          onError();
        }
      }, 3000);
    }
  }, [state.loading])

  if (!state.deleted && !state.confirm) {
    return (
      <div>
        <h2>Eliminar {name}</h2>
        <p>Por favor, escriba el código de seguridad.</p>
  
        {(state.error && !state.loading) && (
          <p>Error: El codigo es incorrecto</p>
        )}
  
        {state.loading && (
          <p>Cargando ...</p>
        )}
  
        <input 
          type='text' 
          placeholder='código de seguridad'
          value={state.value}
          onChange={(event) => {
            // dispatch({ type: actionTypes.write, payload: event.target.value });
            onWrite(event.target.value);
          }}  
        />
        <button
          onClick={() => {
            // dispatch({ type: actionTypes.check });
            onCheck();
          }}
        >Comprobar</button>
      </div>
    )
  } else if (!!state.confirm && !state.deleted) {
    return (
      <>
        <p>¿Segudo que quieres eliminar useState?</p>
        <button 
          onClick={() => {
            // dispatch({ type: actionTypes.delete });
            onDelete();
          }}
        >Si, eliminar</button>
        <button
          onClick={() => {
            // dispatch({ type: actionTypes.reset });
            onReset();
          }}
        >No, regresar</button>
      </>
    )
  } else {
    return (
      <>
        <p>Eliminado con exito</p>
        <button
          onClick={() => {
            // dispatch({ type:  actionTypes.reset });
            onReset();
          }}
        >Volver al useState</button>
      </>
    )
  }
};



const initialState = {
  value: '',
  error: false,
  loading: false,
  deleted: false,
  confirm: false
}

const actionTypes = {
  confirm: 'CONFIRM',
  error: 'ERROR',
  write: 'WRITE',
  delete: 'DELETE',
  check: 'CHECK',
  reset: 'RESET',
}

const reducerObj = (state, payload) => ({
  [actionTypes.confirm]: {
    ...state,
    loading: false,
    error: false,
    confirm: true
  },
  [actionTypes.error]: {
    ...state,
    error: true,
    loading: false,
  },
  [actionTypes.write]: {
    ...state,
    value: payload
  },
  [actionTypes.delete]: {...state,deleted: true},
  [actionTypes.check]: {
    ...state,
    loading: true
  },
  [actionTypes.reset]: {...state,confirm: false, deleted: false, value: ''}
});

const reducer = (state, action) => {
  if(reducerObj(state)[action.type]) {
    return reducerObj(state, action.payload)[action.type];
  } else {
    return state;
  }
}

export {UseReducer}